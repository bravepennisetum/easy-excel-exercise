package com.xydream.easyexcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;
import com.alibaba.excel.read.builder.ExcelReaderSheetBuilder;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import easyexcel.EasyExcelListener;
import model.Student;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExcelTest {


    /**
     * 读取Excel
     */
    @Test
    public void test01() {
        /**
         * 构建一个读的工作牌对象
         *
         * @param pathName  要读的文件的路径
         * @param head      文件中每一行数据要储存的实体的类型的class
         * @param readListener  读监听器，每读一行内容，都会调用一次该对象的invoke，在invoke可以操作使用读取到的数据
         * @return Excel reader builder
         */
        // 获得一个工作簿对象
        ExcelReaderBuilder readWorkBook = EasyExcel.read("easyExcel.xlsx", Student.class, new EasyExcelListener());
        // 获得一个工作表对象
        ExcelReaderSheetBuilder sheet = readWorkBook.sheet();
        // 读取工作表中的内容
        sheet.doRead();
    }

    /**
     * 写入Excel
     */
    @Test
    public void test02() {
        /**
         * 构建一个读的工作牌对象
         *
         * @param pathName  要读的文件的路径
         * @param head      文件中每一行数据要储存的实体的类型的class
         * @param readListener  读监听器，每读一行内容，都会调用一次该对象的invoke，在invoke可以操作使用读取到的数据
         * @return Excel reader builder
         */
        // 获得一个工作簿对象
         ExcelWriterBuilder writeWorkBook = EasyExcel.write("easyExcel-write.xlsx", Student.class);
        // 获得一个工作表对象
        ExcelWriterSheetBuilder sheet = writeWorkBook.sheet();
        // 准备数据
         List<Student> students = initDate();
        // 读取工作表中的内容
        sheet.doWrite(students);
    }

    /**
     * 初始化数据
     * @return
     */
    private static List<Student> initDate() {
        ArrayList<Student> students = new ArrayList<Student>();
        Student data = new Student();
        for (int i = 0; i < 10; i++) {
            data.setName("学号000" + 1);
            data.setGender("男");
            data.setBirthday(new Date());;
            students.add(data);
        }
        return students;
    }

}
