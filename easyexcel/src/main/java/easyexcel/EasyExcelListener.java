package easyexcel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import model.Student;

public class EasyExcelListener  extends AnalysisEventListener<Student> {

    // 每读一样，会调用该invoke方法一次
    public void invoke(Student student, AnalysisContext analysisContext) {
        System.out.println("student" + student);
    }

    // 全部读完之后，会调用该方法
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
