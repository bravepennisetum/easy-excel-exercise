package model;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.util.Date;

/**
 * 学生实体类
 * @ContentRowHeight  设置内容的行高
 * @HeadRowHeight     设置表头的行高
 */


@Data
public class Student {

    /**
     * @ExcelProperty 设置列表的列头
     *  value 对应Excel表中的列头
     *  index 对应Excel表中的列数，默认-1，建议指定时从0开始
     * @ColumnWidth 设置列的列宽
     * @ExcelIgnore 设置该字段不参与读写
     */
    @ExcelProperty(value = "学生姓名", index = 1)
    @ColumnWidth(20)
    private String name;        // 学生姓名

    @ExcelProperty(value = "学生性别", index = 3)
    @ColumnWidth(15)
    private String gender;      // 学生性别

    @ExcelProperty(value = "年龄", index = 2)
    @ColumnWidth(20)
    private Date birthday;      // 学生年龄

    @ExcelProperty(value = "ID", index = 0)
    private String id;          // 学生ID
}
